#include "FastLED.h"
FASTLED_USING_NAMESPACE


#define DATA_PIN    5
#define LED_TYPE    WS2811
#define COLOR_ORDER GRB
#define NUM_LEDS    140
#define CHAMBER 15
#define SIDE 35
#define FRAMES_PER_SECOND  120
#define BUTTON_FASTER 11
#define BUTTON_SLOWER 12
#define BUTTON_LOW 10

CRGB leds[NUM_LEDS];

int toponly = SIDE - 2 * CHAMBER;
unsigned long last = 0;
unsigned long wait = 0;
int counter = 0;

void setup() {
  last = millis();
  delay(1000); // 1 second delay for recovery
  Serial.begin(9600);
  
  // tell FastLED about the LED strip configuration
  FastLED.addLeds<LED_TYPE,DATA_PIN,COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalSMD5050).setTemperature(Halogen);

  // set master brightness control
  FastLED.setBrightness(255);
  Serial.println(toponly);

  pinMode(BUTTON_LOW, OUTPUT);
  digitalWrite(BUTTON_LOW, LOW);
  pinMode(BUTTON_FASTER,INPUT);
  digitalWrite(BUTTON_FASTER,HIGH);
  pinMode(BUTTON_SLOWER,INPUT);
  digitalWrite(BUTTON_SLOWER,HIGH);
  
}

int pulse_interval = 200;

void loop() {
  if (millis() > last + 20 && millis() > wait) {
    while( Serial.available() > 0){
      int commandByte = Serial.read();
      if( commandByte == '-' ){
	Serial.println("Decreasing Speed");
	pulse_interval += 100;
      }
      if( commandByte == '+' ){
	Serial.println("Increasing Speed");      
	pulse_interval = pulse_interval >= 100 ? pulse_interval - 100 : 0;
      }    
    }

    if( !digitalRead(BUTTON_SLOWER)){
      Serial.println("Decreasing Speed");
      pulse_interval += 100;
    }
      
    if( !digitalRead(BUTTON_FASTER)){
      Serial.println("Increasing Speed");      
      pulse_interval = pulse_interval >= 100 ? pulse_interval - 100 : 0;
    }
      

    movePhotons();
    if ( counter == 0 ){
      wait = millis() + pulse_interval;
      while( millis() < wait){
        FastLED.show();  
        FastLED.delay(1000/FRAMES_PER_SECOND);   
	fadePhotons();
      }
    }    
    FastLED.show();  
    FastLED.delay(1000/FRAMES_PER_SECOND); 
    fadePhotons();
    last = millis();
  }
}
void fadePhotons(){
  for( int i=0; i < NUM_LEDS; i++){
    if (
	(i >= 11 && i <= 16) ||
	(i >= 53 && i <= 58) ||
	(i >= 82 && i <= 86) ||
	(i >= 123 && i <= 128)
	){
      leds[i] += CRGB(3, 3, 1);
      leds[i].fadeToBlackBy(60);
    }else{
      leds[i].fadeLightBy(80);
    }
  }

}
void movePhotons(){
      int i = counter;
      for (int j = 0; j < 4; j++) {
        // even sides
        if (j % 2 == 0) {
          int bottom = j * SIDE;
          int top = bottom + SIDE;
  
          if (i > SIDE - CHAMBER - 2) {
            white(bottom + i - toponly);
            white(top - i);
            white(bottom + i - toponly + 1);
            white(top - i - 1);
          }
          else {
            if (i > toponly) {
              blue(bottom + i - toponly);
            }
            blue(top - i);
          }
        }
        // odd sides
        else {
          int top = j * SIDE - 1;
          int bottom = top + SIDE;
  
          if (i > SIDE - CHAMBER -2) {
            white(bottom - i + toponly);
            white(top + i);
            white(bottom - i + toponly - 1);
            white(top + i + 1);
          }
          else {
            if (i > toponly) {
              blue(bottom - i + toponly);
            }
            blue(top + i);
          }
        }
      }
      if (counter >= SIDE - CHAMBER) {
        counter = 0;
      }
      else {
        counter++;
      }
}
void blue(int pos) {
  leds[pos] = CRGB(0,0, 255);
}

void white(int pos) {
  //leds[pos] = CRGB(255, 180, 180);
  leds[pos] = CRGB(255, 255, 255);
  leds[pos].maximizeBrightness();
}


